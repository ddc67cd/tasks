"""Demonstrates work."""
import requests
from time import sleep

url = "http://localhost:8081"


task_ids = []
key = "py36"
print("Schedule 2 tasks with key %s" % key)
for _ in range(2):
    resp = requests.post(url + "/tasks", json={"key": key, "kwargs": dict(x=1, y=1, z=5)})
    task_ids.append(resp.json()["task_id"])

key = "py38"
print("Schedule 2 tasks with key %s" % key)
for _ in range(2):
    resp = requests.post(url + "/tasks", json={"key": key, "kwargs": dict(x=1, y=1, z=5)})
    task_ids.append(resp.json()["task_id"])

sleep(1)  # wail till worker pickup the tasks

print("\nFetch statuses. Some tasks should be IN FLIGHT at this moment.")
for task_id in task_ids:
    resp = requests.get(url + "/tasks/%d" % task_id)
    print("Task %s, status=%s" % (task_id, resp.json()["status"]))

print("\nWait for 10 seconds...")
sleep(10)

print("\nFetch statuses. All tasks should be DONE at this moment.")
for task_id in task_ids:
    resp = requests.get(url + "/tasks/%d" % task_id)
    print("Task %s, status=%s" % (task_id, resp.json()["status"]))

print("\nFetch results")
for task_id in task_ids:
    resp = requests.get(url + "/tasks/%d/result" % task_id)
    print("Task %s, result=%s" % (task_id, resp.text))


print("\nShcedule task with unexpected key 'py39'")
key = "py39"
resp = requests.post(url + "/tasks", json={"key": key, "kwargs": dict(x=1, y=1, z=5)})
task_id = resp.json()["task_id"]
resp = requests.get(url + "/tasks/%d" % task_id)
print("Task %s, status=%s" % (task_id, resp.json()["status"]))


print("\nWait till task will be failed due to missing workers")
sleep(15)
resp = requests.get(url + "/tasks/%d" % task_id)
print("Task %s, status=%s" % (task_id, resp.json()["status"]))
resp = requests.get(url + "/tasks/%d/result" % task_id)
print("Task %s, result=%s" % (task_id, resp.text))
