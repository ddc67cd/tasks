"""Http interface for scheduler."""
import asyncio
import logging
from os import getenv

from aiohttp import web

from scheduler import Scheduler, MISSING
from db import Db, Task

logging.basicConfig(level=logging.INFO)
SCHEDULER_PORT = int(getenv("SCHEDULER_PORT", 8081))
SCHEDULER_HOST = getenv("SCHEDULER_HOST", "0.0.0.0")


async def get_result(request):
    """Http handler for GET /task/<task_id>/result."""
    task_id = request.match_info["task_id"]
    db = request.app["db"]
    result = db.get_result(task_id=task_id)

    if not result:
        return web.HTTPNotFound()

    return web.json_response(result)


async def get_task(request):
    """Http handler for GET /tasks/<task_id>."""
    task_id = request.match_info["task_id"]
    db = request.app["db"]
    task = db.get_task(task_id=task_id)

    if not task:
        return web.HTTPNotFound()

    result = {"task_id": task.id, "status": task.status}
    return web.json_response(result)


async def create_task(request):
    """
    Http handler for POST /tasks.
    Doesn't verify data for simplicity.
    """
    request_json = await request.json()
    key = request_json["key"]
    kwargs = request_json["kwargs"]

    db = request.app["db"]
    task_id = db.create_task(key=key, kwargs=kwargs or {})

    return web.json_response({"task_id": task_id})


async def heartbeat(request):
    """Http handler for POST /heartbeat."""
    scheduler = request.app["scheduler"]
    request_json = await request.json()
    command = scheduler.handle_worker_heartbeat(
        request_json["name"],
        request_json["key"],
        request_json["status"],
        request_json["version"],
        result=request_json.get("result", MISSING),
        exception=request_json.get("exception", MISSING),
    )
    return web.json_response(command)


async def shutdown_app(app):
    await app["scheduler"].close()


async def create_app():
    """Fabric for app with async code."""
    app = web.Application()
    app.router.add_post("/heartbeat", heartbeat)
    app.router.add_post("/tasks", create_task)
    app.router.add_get("/tasks/{task_id}/result", get_result)
    app.router.add_get(r"/tasks/{task_id}", get_task)
    db = app["db"] = Db()
    app["scheduler"] = Scheduler(db)
    app.on_shutdown.append(shutdown_app)

    return app


if __name__ == "__main__":
    web.run_app(
        create_app(), host=SCHEDULER_HOST, port=SCHEDULER_PORT, access_log=False
    )
