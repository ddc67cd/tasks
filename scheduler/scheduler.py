import asyncio
import logging
import json
from typing import Optional
from dataclasses import dataclass
from datetime import datetime, timedelta

from db import Task

MISSING = object()


@dataclass
class Worker:
    name: str
    key: str  # stands for "routing key"
    version: int  # worker generation used to detect worker restarts
    last_seen: datetime
    task: Task = None


class Scheduler:
    """Track workers and tasks."""

    def __init__(self, db):
        self.db = db
        self.workers = {}
        self.watchdog_task = asyncio.create_task(self.watchdog())

    async def close(self):
        """Cleanup resources."""
        self.watchdog_task.cancel()

    def start_task(self, task):
        """Mark task as started."""
        task.status = "IN FLIGHT"
        self.db.save_task(task)

    def fail_task(self, task, error=None, traceback=None):
        """Mark task as failed."""
        task.status = "FAILED"
        self.db.save_task(task)
        self.db.save_result(task.id, result=None, exception=error, traceback=traceback)

    def succeed_task(self, task, result):
        """Mark task as succeeded."""
        task.status = "DONE"
        self.db.save_task(task)
        self.db.save_result(task.id, result=json.dumps(result))

    async def watchdog(self):
        """Handle workers that dont send heartbeats, handle tasks that has no workers to execute."""
        while True:
            available_keys = set()
            dead_workers = []
            for name, worker in self.workers.items():
                if datetime.now() - worker.last_seen >= timedelta(seconds=10):
                    if worker.task:
                        self.fail_task(worker.task, error="Worker disconnected")
                    dead_workers.append(worker.name)
                else:
                    available_keys.add(worker.key)

            # Bury disconnected workers
            for name in dead_workers:
                logging.info("Bury worker %s" % name)
                del self.workers[name]

            # Fail tasks without workers
            keys = self.db.get_pending_keys()
            to_fail = set(keys) - available_keys
            for key in to_fail:
                while True:
                    task = self.db.get_pending_task(key)
                    if not task:
                        break
                    self.fail_task(task, error="No worker available")
                    await asyncio.sleep(0)  # avoid locking

            await asyncio.sleep(10)

    def register_worker(self, name: str, key: str, version: int) -> Worker:
        """Register connected worker."""
        logging.info("New worker '%s' with key '%s' has connected" % (name, key))
        worker = self.workers[name] = Worker(
            name, key, last_seen=datetime.now(), version=version
        )
        return worker

    def handle_worker_heartbeat(
        self, name, key, status, version, result, exception
    ) -> Optional[dict]:
        """Process worker's status and return command if needed."""
        worker = self.workers.get(name)

        if not worker:
            worker = self.register_worker(name, key, version)
            if status == "BUSY":
                return {"command": "cancel"}
        else:
            if worker.version != version:
                logging.warning("Worker %s restarted" % name)
                if worker.task:
                    self.fail_task(worker.task, error="Worker restarted")
                    worker.task = None
                worker.version = version

        worker.last_seen = datetime.now()

        # Handle task finish
        if exception is not MISSING:
            self.fail_task(
                worker.task, error=exception["error"], traceback=exception["traceback"]
            )
            worker.task = None
        elif result is not MISSING:
            self.succeed_task(worker.task, result=result)
            worker.task = None

        # Check wheter worker can proceed next task
        if not worker.task:
            task = self.db.get_pending_task(worker.key)
            if not task:
                return

            worker.task = task
            self.start_task(task)

            return {
                "command": "execute",
                "kwargs": task.kwargs,
            }
