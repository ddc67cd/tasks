from typing import Optional, Tuple, Dict, Literal

import dataset
from dataclasses import asdict, dataclass


Status = Literal["NOT RUN YET", "IN FLIGHT", "DONE", "FAILED"]


@dataclass
class Task:
    key: str
    kwargs: Dict
    status: Status = "NOT RUN YET"
    id: Optional[int] = None


class Db:
    def __init__(self):
        try:
            db = self.db = dataset.connect("sqlite:///volume/db.sqlite3")
        except Exception as exc:
            raise RuntimeError("db not found: %s" % exc) from None

        db.create_table("tasks", primary_id="id")
        db.create_table("results", primary_id="task_id")

    def create_task(self, key, kwargs):
        pk = self.db["tasks"].insert(
            {"kwargs": kwargs, "status": "NOT RUN YET", "key": key,}
        )
        return pk

    def save_task(self, task):
        self.db["tasks"].update(asdict(task), ["id"])

    def get_pending_task(self, key):
        data = self.db["tasks"].find_one(key=key, status="NOT RUN YET", order_by="id")
        if not data:
            return None
        return Task(**data)

    def get_pending_keys(self):
        return [item["key"] for item in self.db["tasks"].distinct("key")]

    def get_task(self, task_id):
        data = self.db["tasks"].find_one(id=task_id)
        if not data:
            return None
        return Task(**data)

    def save_result(self, task_id, result, exception=None, traceback=None):
        self.db["results"].insert(
            dict(
                task_id=task_id, result=result, exception=exception, traceback=traceback
            )
        )

    def get_result(self, task_id):
        data = self.db["results"].find_one(task_id=task_id)
        if not data:
            return None
        return data
