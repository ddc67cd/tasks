import logging
import asyncio
import socket
import sys
import traceback
import os
import json
from concurrent.futures.process import BrokenProcessPool, ProcessPoolExecutor
from time import time

import aiohttp
import dill as pickle

# config
logging.basicConfig(level=logging.INFO)
HEARTBEAT_INTERVAL = int(os.getenv("HEARTBEAT_INTERVAL", 1))
SCHEDULER_ADDRESS = os.getenv("SCHEDULER_ADDRESS", "http://scheduler:8081")
TASK = os.getenv("TASK", "py36_task.dill")
KEY = os.getenv("KEY", "default")


def _run(**kwargs):
    """
    Wraps execution.
    Returns dictionary, ugly way to distinguish between successful result and exception during execution.
    """
    try:
        with open(TASK, "rb") as f:
            run = pickle.loads(f.read())
        res = run(**kwargs)
        return {
            "result": json.dumps(res)
        }
    except Exception as exc:
        return {
            "error": str(exc),
            "traceback": "".join(traceback.format_exception(*sys.exc_info())),
        }


class Worker:
    """
    Simple worker thats capable of running single task in the separate process.
    """

    def __init__(self):
        self.name = socket.gethostname()  # assume we"re running only 1 worker per host
        self.client = aiohttp.ClientSession(base_url=SCHEDULER_ADDRESS)
        self.executor = ProcessPoolExecutor(max_workers=1)
        self.task = None
        self.version = int(time())

    async def close(self):
        logging.info("Stopping the worker")
        await self.client.close()
        self.executor.shutdown()

    def execute(self, kwargs):
        """Submit the task to executor."""
        logging.info("Execute task with %s" % kwargs)
        self.task = self.executor.submit(_run, **kwargs)

    async def cancel(self):
        """Handler for cancel command."""
        logging.info("Reset by request")
        if not self.task:
            return

        await self.task.cancel()
        self.task = None

    def get_status(self):
        """Status indicates whether worker is executing the task or waiting for the command."""
        if self.task and not self.task.done():
            return "BUSY"

        return "IDLE"

    async def run(self):
        """Worker loop."""
        while True:
            try:
                await self.heartbeat()
            except Exception as exc:
                logging.error(exc)

            await asyncio.sleep(HEARTBEAT_INTERVAL)

    async def heartbeat(self):
        """
        Send current state and receive commands.

        Simplifies data exchange between worker and application.
        """
        payload = {
            "name": self.name,
            "key": KEY,
            "status": self.get_status(),
            "version": self.version,
        }

        if self.task and self.task.done():
            try:
                result = self.task.result()
            except BrokenProcessPool:
                # convert failure to fail-stop
                logging.error("Executor has died, exiting...")
                sys.exit(1)

            result = self.task.result()
            if "error" in result:
                payload["exception"] = result
            else:
                payload["result"] = result["result"]

        # Push the state to the scheduler and receive the command
        async with self.client.post("/heartbeat", json=payload) as resp:
            data = await resp.json()

        # task result was successfully sent, time to forget it
        if self.task and self.task.done():
            self.task = None

        if not data:
            # No command was sent
            return

        command = data.get("command")

        if command == "execute":
            self.execute(data["kwargs"])
        elif command == "cancel":
            await self.cancel()
        else:
            logging.warning("Scheduler sent unknown command: %s" % command)


async def main():
    """Entrypoint for worker."""
    worker = Worker()

    try:
        await worker.run()
    finally:
        await worker.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        loop.close()
