Simple tasks service.


To run:
`docker compose up`


To run demo (after starting the system):
`docker compose exec scheduler python test.py`


There are still a lot of work required (not having enough time):
- tracebacks with dill shows no source
- workers are not stopping normally when running in container
- persistence is awkward
- workers code is the same
- etc
